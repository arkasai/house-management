FROM node:18-bullseye as builder

#USER node
WORKDIR /usr/app/src

COPY . /usr/app/src/

WORKDIR /usr/app/src/frontend
RUN npm ci && npm run build:prod

WORKDIR /usr/app/src/backend
RUN npm ci && npm run build

# ---

FROM node:18-bullseye

ENV NODE_ENV production

#USER node
WORKDIR /usr/app/src

COPY --from=builder /usr/app/src/backend/package*.json /usr/app/src/
COPY --from=builder /usr/app/src/backend/dist/ /usr/app/src/dist/
COPY --from=builder /usr/app/src/frontend/dist/frontend/ /usr/app/src/dist/client/

RUN npm ci --only=production

EXPOSE 3000

CMD ["node", "dist/main.js"]
