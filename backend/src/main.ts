import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

/*const allowedOrigins = [
  'http://localhost',
  'http://localhost:3000',
  'http://localhost:4200',
  'http://192.168.100.9:3000',
];

const corsOptions = {
  origin: (origin, callback): void => {
    if (allowedOrigins.includes(origin) || !origin) {
      callback(null, true);
    } else {
      callback(new Error(`Origin "${origin}" not allowed by CORS`));
    }
  },
};*/

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  // app.enableCors(corsOptions);
  app.useGlobalPipes(new ValidationPipe());

  const options = new DocumentBuilder()
    .setTitle('House Management')
    .setDescription('API to provide CRUD environment for house data')
    .setVersion('0.1.1')
    .build();

  SwaggerModule.setup('api', app, SwaggerModule.createDocument(app, options));

  await app.listen(3000);
}

bootstrap();
