import { Injectable } from '@nestjs/common';
import { ConsumptionEntity } from './entities/consumption.entity';
import { CreateConsumptionDto } from './dto/create-consumption.dto';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class ConsumptionService {
  private readonly dirPath = path.join(__dirname, '../data');
  private readonly filePath = path.join(this.dirPath, '/data.json');

  create(consumption: CreateConsumptionDto): ConsumptionEntity {
    if (!fs.existsSync(this.dirPath)) {
      fs.mkdirSync(this.dirPath);
    }

    const consumptions: ConsumptionEntity[] = this.findAll();
    consumptions.push(consumption);

    fs.writeFileSync(this.filePath, JSON.stringify(consumptions));

    return consumption;
  }

  findAll(): ConsumptionEntity[] {
    try {
      return JSON.parse(
        fs.readFileSync(this.filePath).toString(),
      ) as ConsumptionEntity[];
    } catch (err) {
      console.error(err);
      return [];
    }
  }
}
