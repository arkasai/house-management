import {
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  Post,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ConsumptionService } from './consumption.service';
import { CreateConsumptionDto } from './dto/create-consumption.dto';
import { ConsumptionEntity } from './entities/consumption.entity';

@ApiTags('consumption')
@Controller('consumption')
export class ConsumptionController {
  constructor(private readonly houseService: ConsumptionService) {}

  @Post()
  @ApiOperation({
    summary: 'Create entry for monthly consumption of the house',
  })
  async create(
    @Body() newConsumption: CreateConsumptionDto,
  ): Promise<ConsumptionEntity> {
    try {
      return this.houseService.create(newConsumption);
    } catch (err) {
      console.error('Error', err.stack);
      console.error('Error', err.name);
      console.error('Error', err.message);
      throw new InternalServerErrorException();
    }
  }

  @Get()
  @ApiOperation({
    summary: 'Get all consumptions of the house one a montly basis',
  })
  findAll(): ConsumptionEntity[] {
    return this.houseService.findAll();
  }
}
