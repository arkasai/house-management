import { ApiProperty } from '@nestjs/swagger';

export class ConsumptionEntity {
  @ApiProperty({ type: Date })
  date: string;
  gas: number;
  water: number;
  electricityDay: number;
  electricityNight: number;
  correctionGas?: number;
  correctionWater?: number;
  correctionElectricityDay?: number;
  correctionElectricityNight?: number;
}
