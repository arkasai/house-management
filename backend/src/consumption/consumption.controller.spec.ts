import { Test, TestingModule } from '@nestjs/testing';
import { ConsumptionController } from './consumption.controller';
import { ConsumptionService } from './consumption.service';
import { ConsumptionEntity } from './entities/consumption.entity';
import { CreateConsumptionDto } from './dto/create-consumption.dto';

describe('HouseController', () => {
  let controller: ConsumptionController;
  let service: ConsumptionService;
  let consoleErrorSpy;

  beforeAll(async () => {
    consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
  });

  afterAll(async () => {
    consoleErrorSpy.mockRestore();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConsumptionController],
      providers: [ConsumptionService],
    }).compile();

    service = module.get<ConsumptionService>(ConsumptionService);
    controller = module.get<ConsumptionController>(ConsumptionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of consumptions', async () => {
      const result: ConsumptionEntity[] = [
        {
          date: new Date().toISOString(),
          gas: 123.0,
          water: 234,
          electricityDay: 0.123,
          electricityNight: 0.123,
        },
      ];

      jest.spyOn(service, 'findAll').mockImplementation(() => result);
      expect(await controller.findAll()).toBe(result);
    });
  });

  describe('create', () => {
    it('should create a new consumption entry', async () => {
      const result: CreateConsumptionDto = {
        date: new Date().toISOString(),
        gas: 123.0,
        water: 234,
        electricityDay: 0.123,
        electricityNight: 0.123,
      };

      jest.spyOn(service, 'create').mockImplementation(() => result);
      expect(await controller.create(result)).toBe(result);
    });

    it('should throw internal server error exception if error occurs', async () => {
      const result: CreateConsumptionDto = {
        date: new Date().toISOString(),
        gas: 123.0,
        water: 234,
        electricityDay: 0.123,
        electricityNight: 0.123,
      };

      jest.spyOn(service, 'create').mockImplementation(() => {
        throw new Error('Could not access file');
      });
      expect(async () => {
        await controller.create(result);
      }).rejects.toEqual(new Error('Internal Server Error'));
    });
  });
});
