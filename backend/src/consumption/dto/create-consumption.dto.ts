import { IsDateString, IsNumber, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateConsumptionDto {
  @ApiProperty({ type: Date })
  @IsDateString()
  readonly date: string;

  @IsNumber()
  readonly gas: number;

  @IsNumber()
  readonly water: number;

  @IsNumber()
  readonly electricityDay: number;

  @IsNumber()
  readonly electricityNight: number;

  @IsOptional()
  @IsNumber()
  readonly correctionGas?: number;

  @IsOptional()
  @IsNumber()
  readonly correctionWater?: number;

  @IsOptional()
  @IsNumber()
  readonly correctionElectricityDay?: number;

  @IsOptional()
  @IsNumber()
  readonly correctionElectricityNight?: number;
}
