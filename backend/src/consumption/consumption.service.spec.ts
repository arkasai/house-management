import { Test, TestingModule } from '@nestjs/testing';
import { ConsumptionService } from './consumption.service';
import { ConsumptionEntity } from './entities/consumption.entity';
import * as fs from 'fs';

describe('HouseService', () => {
  let service: ConsumptionService;
  let consoleErrorSpy;
  jest.mock('fs');

  beforeAll(async () => {
    consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation();
  });

  afterAll(async () => {
    consoleErrorSpy.mockRestore();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConsumptionService],
    }).compile();

    service = module.get<ConsumptionService>(ConsumptionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of consumptions', async () => {
      const result: ConsumptionEntity[] = [
        {
          date: new Date().toISOString(),
          gas: 123.0,
          water: 234,
          electricityDay: 0.123,
          electricityNight: 0.123,
        },
      ];
      jest
        .spyOn(fs, 'readFileSync')
        .mockImplementation(() => JSON.stringify(result));
      expect(await service.findAll()).toStrictEqual(result);
    });
  });

  describe('create', () => {
    it('should create a new consumption entry', async () => {
      const result: ConsumptionEntity = {
        date: new Date().toISOString(),
        gas: 123.0,
        water: 234,
        electricityDay: 0.123,
        electricityNight: 0.123,
      };

      jest.spyOn(fs, 'writeFileSync').mockImplementation();
      expect(await service.create(result)).toBe(result);
    });

    it('should throw internal server error exception if error occurs', async () => {
      const result: ConsumptionEntity = {
        date: new Date().toISOString(),
        gas: 123.0,
        water: 234,
        electricityDay: 0.123,
        electricityNight: 0.123,
      };

      jest.spyOn(fs, 'writeFileSync').mockImplementation(() => {
        throw new Error('Could not access file');
      });
      expect(async () => {
        await service.create(result);
      }).rejects.toEqual(new Error('Could not access file'));
    });
  });
});
