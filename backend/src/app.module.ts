import { Module } from '@nestjs/common';
import { join } from 'path';
import { ConsumptionModule } from './consumption/consumption.module';
import { ServeStaticModule } from '@nestjs/serve-static';

@Module({
  imports: [
    ConsumptionModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, 'client'),
      exclude: ['/api*'],
    }),
  ],
})
export class AppModule {}
