import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { ConsumptionService } from '../src/consumption/consumption.service';
import { CreateConsumptionDto } from '../src/consumption/dto/create-consumption.dto';

describe('ConsumptionController (e2e)', () => {
  let app: INestApplication;
  const service = {
    findAll: () => [
      {
        date: '2020-12-06T00:00:00.933Z',
        gas: 123.0,
        water: 234,
        electricityDay: 0.123,
        electricityNight: 0.123,
      },
    ],
    create: (data) => data,
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConsumptionService)
      .useValue(service)
      .compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('/consumption (GET) 200', () => {
    return request(app.getHttpServer())
      .get('/consumption')
      .expect(HttpStatus.OK)
      .expect(service.findAll());
  });

  it('/consumption (POST) 200', () => {
    const consumption: CreateConsumptionDto = {
      date: new Date().toISOString(),
      gas: 0.123,
      water: 0.123,
      electricityDay: 4321,
      electricityNight: 987,
    };
    return request(app.getHttpServer())
      .post('/consumption')
      .set('Accept', 'application/json')
      .send(consumption)
      .expect(HttpStatus.CREATED);
  });

  it('/consumption (POST) 400', () => {
    return request(app.getHttpServer())
      .post('/consumption')
      .set('Accept', 'application/json')
      .send({})
      .expect(HttpStatus.BAD_REQUEST);
  });
});
