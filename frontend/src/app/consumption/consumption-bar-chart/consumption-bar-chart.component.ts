import { Component, Input, OnInit } from '@angular/core';
// @ts-ignore
import { EChartOption } from 'echarts';

@Component({
  selector: 'app-consumption-bar-chart',
  templateUrl: './consumption-bar-chart.component.html',
  styleUrls: ['./consumption-bar-chart.component.css']
})
export class ConsumptionBarChartComponent implements OnInit {
  @Input() labels: string[] = [];
  @Input() color: string;
  @Input() name: string;
  @Input() data: number[];
  @Input() unit: string;

  barChart: EChartOption;

  ngOnInit(): void {
    this.barChart = {
      color: [this.color],
      legend: {
        data: [this.name]
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        },
        formatter: '{a0}: {c0} ' + this.unit
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: this.labels
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: this.name,
          data: this.data,
          type: 'bar'
        }
      ]
    };
  }
}
