import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumptionBarChartComponent } from './consumption-bar-chart.component';
import { ConsumptionService } from '../service/consumption.service';
import { of } from 'rxjs';

describe('ConsumptionBarChartComponent', () => {
  let component: ConsumptionBarChartComponent;
  let fixture: ComponentFixture<ConsumptionBarChartComponent>;

  beforeEach(async () => {
    const consumptionService = jasmine.createSpyObj('ConsumptionService', ['getConsumption']);
    const getDataSpy = consumptionService.getConsumption.and.returnValue(of(TEST_DATA));

    await TestBed.configureTestingModule({
      declarations: [
        ConsumptionBarChartComponent
      ],
      providers: [{provide: ConsumptionService, useValue: consumptionService}]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumptionBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const TEST_DATA = [
  {
    date: '2020-12-13T13:06:45.403Z',
    gas: 260,
    water: 125,
    electricityDay: 2100,
    electricityNight: 2200
  },
  {
    date: '2020-11-13T13:06:45.403Z',
    gas: 230,
    water: 113,
    electricityDay: 2000,
    electricityNight: 2120
  }
];
