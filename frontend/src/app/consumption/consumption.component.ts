import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, pairwise } from 'rxjs/operators';
import { ConsumptionService } from './service/consumption.service';
import { from } from 'rxjs';
import { ConsumptionDto } from './dto/consumption.dto';

@Component({
  selector: 'app-consumption',
  templateUrl: './consumption.component.html',
  styleUrls: ['./consumption.component.css']
})
export class ConsumptionComponent implements OnInit {
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({matches}) => {
      if (matches) {
        return {
          columns: 1,
          lineChart: {cols: 1, rows: 2},
          barChart: {cols: 1, rows: 1},
          table: {cols: 1, rows: 2},
        };
      }

      return {
        columns: 4,
        lineChart: {cols: 4, rows: 2},
        barChart: {cols: 2, rows: 2},
        table: {cols: 4, rows: 2},
      };
    })
  );

  legendLineChart: string[] = ['Gas', 'Wasser', 'Strom - Tag', 'Strom - Nacht'];
  tableColumns: string[] = ['date', 'gas', 'water', 'electricityDay', 'electricityNight'];
  labels: string[] = [];
  rawData: ConsumptionDto[] = [];
  gas: number[] = [];
  water: number[] = [];
  electricityDay: number[] = [];
  electricityNight: number[] = [];

  constructor(
    private readonly breakpointObserver: BreakpointObserver,
    private readonly conService: ConsumptionService
  ) {
  }

  ngOnInit(): void {
    this.conService.getConsumption().subscribe(
      data => {
        this.rawData = data;

        const pairs = from(data).pipe(pairwise());
        pairs.pipe(
          map(pair => {
              const correctionGas = pair[1].correctionGas ?? 0;
              const correctionWater = pair[1].correctionWater ?? 0;
              const correctionElectricityDay = pair[1].correctionElectricityDay ?? 0;
              const correctionElectricityNight = pair[1].correctionElectricityNight ?? 0;

              this.labels.push(new Date(pair[1].date).toLocaleDateString('de-DE'));
              this.gas.push(Math.round(pair[1].gas + correctionGas - pair[0].gas));
              this.water.push(Math.round(pair[1].water + correctionWater - pair[0].water));
              this.electricityDay.push(Math.round(pair[1].electricityDay + correctionElectricityDay - pair[0].electricityDay));
              this.electricityNight.push(Math.round(pair[1].electricityNight + correctionElectricityNight - pair[0].electricityNight));
            }
          )
        ).subscribe();
      }
    );
  }
}
