import { Component, Input, OnInit } from '@angular/core';
// @ts-ignore
import { EChartOption } from 'echarts';

@Component({
  selector: 'app-consumption-line-chart',
  templateUrl: './consumption-line-chart.component.html',
  styleUrls: ['./consumption-line-chart.component.css']
})
export class ConsumptionLineChartComponent implements OnInit {
  @Input() legend: string[] = [];
  @Input() labels: string[] = [];
  @Input() gas: number[] = [];
  @Input() water: number[] = [];
  @Input() electricityDay: number[] = [];
  @Input() electricityNight: number[] = [];

  chartOption: EChartOption;

  ngOnInit(): void {
    this.chartOption = {
      legend: {
        data: this.legend
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: this.labels
      },
      yAxis: {
        type: 'value'
      },
      tooltip: {
        trigger: 'axis',
        formatter: '<b>{b}</b><br/>{a0}: {c0} m&sup3; <br/>{a1}: {c1} m&sup3;<br/>{a2}: {c2} kWh<br/>{a3}: {c3} kWh'
      },
      series: [
        {
          name: 'Wasser',
          data: this.water,
          type: 'line'
        },
        {
          name: 'Gas',
          data: this.gas,
          type: 'line'
        },
        {
          name: 'Strom - Tag',
          data: this.electricityDay,
          type: 'line'
        },
        {
          name: 'Strom - Nacht',
          data: this.electricityNight,
          type: 'line'
        }
      ]
    };
  }
}
