import { Injectable } from '@angular/core';
import { ConsumptionDto } from '../dto/consumption.dto';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsumptionService {

  private url = environment.backendUrl + 'api/consumption';

  constructor(
    private readonly http: HttpClient
  ) {
  }

  getConsumption(): Observable<ConsumptionDto[]> {
    return this.http.get<ConsumptionDto[]>(this.url);
  }

  addConsumption(data: ConsumptionDto): Observable<ConsumptionDto> {
    const httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post<ConsumptionDto>(this.url, JSON.stringify(data), {headers: httpHeaders});

  }
}
