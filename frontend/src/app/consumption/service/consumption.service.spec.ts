import { TestBed } from '@angular/core/testing';

import { ConsumptionService } from './consumption.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

describe('ConsumptionService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: ConsumptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ConsumptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
