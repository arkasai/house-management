import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsumptionRoutingModule } from './consumption-routing.module';
import { ConsumptionComponent } from './consumption.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { ConsumptionInputComponent } from './consumption-input/consumption-input.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { ConsumptionLineChartComponent } from './consumption-line-chart/consumption-line-chart.component';
import { ConsumptionBarChartComponent } from './consumption-bar-chart/consumption-bar-chart.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { NgxEchartsModule } from 'ngx-echarts';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { LayoutModule } from '@angular/cdk/layout';
import { CardComponent } from './card/card.component';
import { TableComponent } from './table/table.component';
import { MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
  declarations: [
    ConsumptionComponent,
    ConsumptionInputComponent,
    ConsumptionLineChartComponent,
    ConsumptionBarChartComponent,
    CardComponent,
    TableComponent,
  ],
  imports: [
    CommonModule,
    ConsumptionRoutingModule,
    MatTableModule,
    MatSortModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    LayoutModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    MatPaginatorModule
  ]
})
export class ConsumptionModule {
}
