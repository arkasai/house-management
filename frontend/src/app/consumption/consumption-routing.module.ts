import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsumptionComponent } from './consumption.component';
import { ConsumptionInputComponent } from './consumption-input/consumption-input.component';

const routes: Routes = [
  {path: 'input', component: ConsumptionInputComponent},
  {path: '', component: ConsumptionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsumptionRoutingModule {
}
