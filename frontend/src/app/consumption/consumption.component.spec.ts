import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ConsumptionComponent } from './consumption.component';
import { Component } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { ConsumptionService } from './service/consumption.service';
import { of } from 'rxjs';

describe('ConsumptionComponent', () => {
  let component: ConsumptionComponent;
  let fixture: ComponentFixture<ConsumptionComponent>;

  beforeEach(waitForAsync(() => {
    const consumptionService = jasmine.createSpyObj('ConsumptionService', ['getConsumption']);
    const getDataSpy = consumptionService.getConsumption.and.returnValue(of(TEST_DATA));

    TestBed.configureTestingModule({
      declarations: [ConsumptionComponent],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatCardModule,
        MatGridListModule,
        MatIconModule,
        MatMenuModule,
      ],
      providers: [{provide: ConsumptionService, useValue: consumptionService}]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});

const TEST_DATA = [
  {
    date: '2020-12-13T13:06:45.403Z',
    gas: 260,
    water: 125,
    electricityDay: 2100,
    electricityNight: 2200
  },
  {
    date: '2020-11-13T13:06:45.403Z',
    gas: 230,
    water: 113,
    electricityDay: 2000,
    electricityNight: 2120
  }
];

@Component({selector: 'app-consumption-table', template: ''})
class ConsumptionTableStubComponent {
}

@Component({selector: 'app-card', template: ''})
class CardStubComponent {
}
