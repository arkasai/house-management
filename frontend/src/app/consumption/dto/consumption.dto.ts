export interface ConsumptionDto {
  date: string;
  gas: number;
  water: number;
  electricityDay: number;
  electricityNight: number;
  correctionGas?: number;
  correctionWater?: number;
  correctionElectricityDay?: number;
  correctionElectricityNight?: number;
}
