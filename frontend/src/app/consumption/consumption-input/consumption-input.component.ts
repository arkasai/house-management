import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ConsumptionDto } from '../dto/consumption.dto';
import { ConsumptionService } from '../service/consumption.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-consumption-input',
  templateUrl: './consumption-input.component.html',
  styleUrls: ['./consumption-input.component.css']
})
export class ConsumptionInputComponent {
  conForm = this.fb.group({
    gas: [0, Validators.required],
    water: [0, Validators.required],
    electricityDay: [0, Validators.required],
    electricityNight: [0, Validators.required]
  });

  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly activeRoute: ActivatedRoute,
    private readonly conService: ConsumptionService
  ) {
  }

  save(): void {
    const consumption: ConsumptionDto = {
      gas: this.conForm.value.gas,
      water: this.conForm.value.water,
      electricityDay: this.conForm.value.electricityDay,
      electricityNight: this.conForm.value.electricityNight,
      date: new Date().toISOString(),
    };

    this.conService.addConsumption(consumption).subscribe(
      data => {
        console.log('Success writing: ', data);
        this.router.navigate(['..'], {relativeTo: this.activeRoute});
      }
    );
  }

  get gas(): AbstractControl | null {
    return this.conForm.get('gas');
  }

  get water(): AbstractControl | null {
    return this.conForm.get('water');
  }

  get electricityDay(): AbstractControl | null {
    return this.conForm.get('electricityDay');
  }

  get electricityNight(): AbstractControl | null {
    return this.conForm.get('electricityNight');
  }
}
