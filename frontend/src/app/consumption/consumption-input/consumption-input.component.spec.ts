import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumptionInputComponent } from './consumption-input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ConsumptionService } from '../service/consumption.service';

describe('ConsumptionInputComponent', () => {
  let component: ConsumptionInputComponent;
  let fixture: ComponentFixture<ConsumptionInputComponent>;

  beforeEach(async () => {
    const consumptionService = jasmine.createSpyObj('ConsumptionService', ['addConsumption']);

    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [ConsumptionInputComponent],
      providers: [{provide: ConsumptionService, useValue: consumptionService}]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumptionInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
